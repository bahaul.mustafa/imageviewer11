/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable prefer-const */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-zoom-component',
  templateUrl: './zoom-component.component.html',
  styleUrls: ['./zoom-component.component.scss'],
})
export class ZoomComponentComponent implements OnInit {
  // @ViewChild('slider', { read: ElementRef }) slider: ElementRef;
  @ViewChild(IonSlides) slides: IonSlides;

  public img = this.navParmas.get('img');
  sliderOptions = {
    zoom: true,
    // zoom: {
    //   maxRatio: 8,
    // },
  };
  constructor(
    private navParmas: NavParams,
    private modlCtrl: ModalController
  ) {}

  ngOnInit() {
    console.log(this.img);
  }

  ionViewDidEnter() {
    this.slides.update();
  }

  // zoom(zoomIn: boolean) {
  //   let zoom = this.slider.nativeElement.zoom;
  //   if (zoomIn) {
  //     zoom.in();
  //   } else {
  //     zoom.out();
  //   }
  // }
  async zoom(zoomIn: boolean) {
    const slider = await this.slides.getSwiper();
    const zoom = slider.zoom;
    zoomIn ? zoom.in() : zoom.out();
  }
  close() {
    this.modlCtrl.dismiss();
  }
}
