/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { ZoomComponentComponent } from '../zoom-component/zoom-component.component';

@Component({
  selector: 'app-test-c',
  templateUrl: './test-c.component.html',
  styleUrls: ['./test-c.component.scss'],
})
export class TestCComponent implements OnInit {
  constructor(private modalCtrl: ModalController) {}
  sliderOptions = {
    // slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    // zoom: true,
  };

  ngOnInit() {}
  slides = [
    'https://images.unsplash.com/photo-1628191011123-e7f6b5ec16a9?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2071&q=80',
    'https://images.unsplash.com/photo-1632297536142-ea8ff747173d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=387&q=80',
    'https://images.unsplash.com/photo-1632347866614-61b43ccf2dda?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=436&q=80',
    'https://images.unsplash.com/photo-1632329385643-b8fb86339108?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=464&q=80',
    'https://images.unsplash.com/photo-1593642531955-b62e17bdaa9c?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=869&q=80',
    'https://images.unsplash.com/photo-1632320666787-152f630a6e10?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=387&q=80',
  ];

  openPreview(img) {
    console.log(img);
    this.modalCtrl
      .create({
        component: ZoomComponentComponent,
        componentProps: {
          // eslint-disable-next-line object-shorthand
          img: img,
        },
      })
      .then((modal) => modal.present());
  }
}
