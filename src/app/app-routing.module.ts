import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TestCComponent } from './test-c/test-c.component';
import { SearchFilterCComponent } from './search-filter-c/search-filter-c.component';
const routes: Routes = [
  { path: 'test-c', component: TestCComponent },
  {
    path: '',
    redirectTo: 'test-c',
    pathMatch: 'full',
  },
  {
    path: 'search-filter',
    component: SearchFilterCComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
