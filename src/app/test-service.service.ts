/* eslint-disable @typescript-eslint/semi */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/type-annotation-spacing */
/* eslint-disable @typescript-eslint/quotes */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { postData } from './home';
import { CATCH_ERROR_VAR } from '@angular/compiler/src/output/output_ast';
@Injectable({
  providedIn: 'root',
})
export class TestServiceService {
  postUrl: string = 'http://localhost:3000/users';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  constructor(private http: HttpClient) {}

  addEmployee(postD: postData) {
    return this.http.post(this.postUrl, postD, this.httpOptions);
  }
}
