/* eslint-disable no-trailing-spaces */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TestCComponent } from './test-c/test-c.component';
import { ZoomComponentComponent } from './zoom-component/zoom-component.component';
import { SearchFilterCComponent } from './search-filter-c/search-filter-c.component';

@NgModule({
  declarations: [
    AppComponent,
    TestCComponent,
    ZoomComponentComponent,
    SearchFilterCComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
  ],
  exports: [TestCComponent, ZoomComponentComponent, SearchFilterCComponent],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
